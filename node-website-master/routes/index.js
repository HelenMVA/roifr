var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs'); /*задекларируем переменную она новая для нас*/ 
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var router = express.Router();

router.get('/principal', function(req, res, next) {
  res.render('principal', { page: 'principal', menuId:'principal'});
});

router.get('/hugues-capet', function(req, res, next) {
  res.render('hugues-capet', {page:'hugues-capet', menuId:'hugues-capet'});
});
router.get('/Phil', function (req, res, next) {
  res.render('Phil', { page: 'Phil', menuId: 'Phil' });
});
router.get('/louisIX', function (req, res, next) {
  res.render('louisIX', { page: 'louisIX', menuId: 'louisIX' });
});
router.get('/bel', function (req, res, next) {
  res.render('bel', { page: 'bel', menuId: 'bel' });
});
router.get('/contact', function(req, res, next) {
  res.render('contact', { page:'contact', menuId:'contact'});
});

router.post("/contact", urlencodedParser, function (req, res) {

  var per = req.body; /*считывает инфу*/
  var vst = JSON.stringify(per,null,2); /*модифицирует инфу из js вjson формат */
  fs.appendFile("candidat.json","\n" + vst,function (err) {
    if (err) throw err;

  })


  console.log(req.body)
  res.render('principal', { page: 'principal', menuId: 'principal' });
});





module.exports = router;

